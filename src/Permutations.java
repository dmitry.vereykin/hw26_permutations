import java.util.ArrayList;
import java.util.Scanner;

public class Permutations {//Start of class

    private static int count = 0;
    private static ArrayList<String> perm = new ArrayList<String>();

    public static void main(String[] args) {//Start of main() method
        Scanner in = new Scanner(System.in);

        System.out.print("Please enter the word to be permuted: ");
        String word = in.next();

        System.out.println("Here are the permutations of that word:");
        perm.add(word);
        permute("", word);

        for (int i = 0; i < perm.size(); i++) {
            System.out.println((i + 1) + "  " + perm.get(i));
        }
        System.out.println("That's all.");

    }//End of main() method

    public static void permute(String left, String right) {//Start of method
        int lenRight = right.length();
        if (lenRight == 0) {//Start of base case
            count++;
            //System.out.printf("%,6d  %s\n", count, left);
        }//End of base case
        else {//recursive case
            for (int n = 0; n < lenRight; n++) {//Start of "for" loop.
                String newLeft = left + right.charAt(n);
                String newRight = right.substring(0, n) + right.substring(n + 1);
                String newWord = newLeft + newRight;
                if (!perm.contains(newWord))
                    perm.add(newWord);
                permute(newLeft, newRight);
            }//End of "for" loop
        }//End of recursive case
    }// }//End of method

}//End of class